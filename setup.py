# -*- coding: utf-8 -*-
import os
from setuptools import Command
from setuptools import setup, find_packages


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./lib ./*.pyc ./*.tgz ./*.egg-info')

        
try:
    long_description = open("README.rst").read()
except IOError:
    long_description = ""

setup(
    name="lemonop-utilities",
    version="1.0",
    description="LemonOp Utilities",
    license="MIT",
    author="Sateesh",
    author_email='sateesh@talentspear.com',
    url='https://talentspear.com',
    packages=find_packages(exclude=["test.*", "test"]),
    package_data={' ': ['*.json', '*.wsgi']},
    install_requires=[
         'Flask==0.10.1',
        'itsdangerous==0.24',
        'Jinja2==2.7.3',
        'MarkupSafe==0.23',
        'Werkzeug==0.10.4',
        'psycopg2==2.6.1',
        'peewee==2.4.7',
       'passlib==1.6.2',
       'pycrypto==2.6.1',
       'incoming==0.3.1',
       'twython==3.3.0',
       'requests==2.13.0',
       'nlp==3.3'
    ],
    long_description=long_description
)

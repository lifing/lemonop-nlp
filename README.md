# Lemonop-NLP
This repository is lemonop-nlp, where natural language processing is done and scores for the same are calculated.  

# Development
### Pre-requisites

Install python 2.7.x (we are not using 3.x)

Install oracle-java8-installer 

Install eclipse-neon

Install pip 

```
easy_install pip
```

Install Scipy 

```
sudo pip install scipy
```

nltk - to download , follow the steps listed in the website: https://pythonprogramming.net/tokenizing-words-sentences-nltk-tutorial/

grammar_check, 3To2, LanguageTool - to download, follow the steps listed in the website: https://pypi.org/project/grammar-check/

Install numpy 

```
sudo pip install numpy
```

Install mongodb - refer https://bitbucket.org/lifing/syzygy-mongodb for more information


### Set up project

fork lemonop-nlp into your personal repository

clone the forked repository (either ssh or https) into your system using terminal window

```
git clone https://<your_bitbuket_id>@bitbucket.org/lifing/lemonop-nlp.git

```

# Install python dependencies using
```
pip install requirements.txt
```

# Running the App
```
python nlp_endpoint.py
```
The above process will make your lemonop-nlp up and running on port 8084


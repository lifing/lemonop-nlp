'''
Created on 1 July 2017

@author: sats
'''
'''
a common method for loading a File 
'''

from flask import Flask
import json
import logging
from logging.config import dictConfig
import os
from pymongo import MongoClient


def load_file(file_name):
    full_path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "."))
    file_path = file_name % os.path.dirname(full_path)
    return file_path


'''
a logger configuration
'''

dictConfig(json.load(open(load_file('%s/config/logger.json'), 'r')))
log = logging.getLogger()

api = json.load(open(load_file('%s/config/api_uri.json'), 'r'))
platform_actions = json.load(open(load_file('%s/config/platform_actions.json'), 'r'))
db_config = json.load(open(load_file('%s/config/databaseProd.json'), 'r'))
env = json.load(open(load_file('%s/config/environment.json'), 'r'))

''' load flask app'''

app = Flask(__name__)
app.config.from_object(__name__)

''' Mongodb connection'''
client = MongoClient(db_config.get('database_host'), db_config.get('database_port'), maxPoolSize=db_config.get('database_max_connections'), maxIdleTimeMS=db_config.get('maxIdleTimeMS'))
db = client[ db_config.get('database_name')]
db.authenticate(db_config.get('database_username'), db_config.get('database_password'))

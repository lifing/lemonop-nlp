'''
Created on Jul 1, 2017

@author: havannavar
'''

'''
    Save a new record into the provided collection
'''

from app import db
import logging

logging.basicConfig()
log = logging.getLogger(__name__)

def save(collection, json_data):
    collectionName = db[collection]
    return collectionName.insert_one(json_data)


'''
    Save all records in the provided collection
'''


def save_all(collection, json_data):
    collectionName = db[collection]
    return collectionName.insert_many(json_data)


'''
    Update an existing record for the given collectionName
'''


def update(collection, json_data, _id):
    try:
        collectionName = db[collection]
        return collectionName.update({'_id':_id}, {"$set": json_data}, upsert=False, multi=False)
    except Exception as e:
        log.warn(e)
        return None
        

'''
    return a single record for the query string
'''


def get(collection, query_params):
    collectionName = db[collection]
    return collectionName.find_one(query_params)


'''
    return a multiple record for the query string 
    return all records for no query condition db.collection.find({})
'''


def get_all(collection, query_params):
    collectionName = db[collection]
    if query_params == None or len(query_params) == 0:        
        return list(collectionName.find({}, {'_class' : False}))
    else:
        return list(collectionName.find(query_params, {'_class' : False}))

'''
    return paginated items from the collection
'''
def get_items(collection, query_params, skip):
    try:
        
        collectionName = db[collection]
        if query_params == None or len(query_params) == 0:        
            # return list(collectionName.find({}, {'_class' : False}).limit(300).sort([{"updatedDateTime", -1}]))
            return list(collectionName.find({}, {'_class' : False}).skip(skip).limit(50))
        else:
            # return list(collectionName.find(query_params, {'_class' : False}).limit(300).sort([{"updatedDateTime", -1}]))
            return list(collectionName.find(query_params, {'_class' : False}).skip(skip).limit(50))
    except Exception as e:
        print e   
        return None 

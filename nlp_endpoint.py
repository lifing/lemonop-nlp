#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding=utf8  
'''
Created on Jul 1, 2017

@author: havannavar
'''

from app import app, api
from nlp import controller

application = app


@app.route(api.get('score'), methods=['POST', 'GET'])
def process_data():
    '''
    
    '''
    return controller.nltk_processor()

@app.route(api.get('gigKeywords'), methods=['POST', 'GET'])
def process_gig():
    '''
    
    '''
    return controller.extract_gig_keywords()


def runserver():
    app.run(host='localhost', port=8084)


if __name__ == '__main__':
    runserver()

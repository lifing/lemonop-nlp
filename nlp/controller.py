'''
Created on May 29, 2018

@author: shricharan
'''
import helper, logging
from flask import make_response, jsonify, request

logging.basicConfig()
log = logging.getLogger(__name__)


def nltk_processor():
    try: 
        if request.method == 'GET':
            helper.parse_all_profiles()
        else:    
            helper.parse_one_profile()
        return make_response(jsonify("Operation successfull"))
    except Exception as e:
        log.error(e)
        return make_response(jsonify({'error':'Invalid request: either your request expired or not a valid request '}), 402)
    
def extract_gig_keywords():
    try: 
        if request.method == 'GET':
            helper.parse_all_profiles()
        else:    
            helper.calculate_gig_keywords()
        return make_response(jsonify("Operation successfull"))
    except Exception as e:
        log.error(e)
        return make_response(jsonify({'error':'Invalid request: either your request expired or not a valid request '}), 402)
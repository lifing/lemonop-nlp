'''
Created on May 29, 2018

@author: shricharan
'''
import requests
import json
from app import platform_actions, api


def get_distance_matrix():
    try:
        headers = {'content-type':'application/json; charset=utf-8'}
         
        response = requests.get(platform_actions.get('GET_MATRIX_TEMPLATE'), headers=headers).json()
        return response[0].get('matrix')
    except Exception as e:
        print e

        
def get_nlp_auth_id():
    try:
        headers = {'content-type':'application/json; charset=utf-8'}
         
        response = requests.get(platform_actions.get('GET_NLP_AUTH_ID'), headers=headers).json()
        return response.get('nlpAuthId')
    except Exception as e:
        print e


def update_gig(gig, nlpAuthId):
    try:
#         append the auth key to the request header
        headers = {'content-type':'application/json; charset=utf-8', 'Authorization':nlpAuthId}
        
#         append gigId to the api endpoint
        api_url = api.get('updateGig') + '/' + gig.get('gigId')
        
        response = requests.put(api_url, data=json.dumps(gig), headers=headers)
        return response
        
    except Exception as e:
        print e

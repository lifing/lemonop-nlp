'''
Created on May 29, 2018

@author: shricharan
'''
# -*- coding: utf-8 -*-
import grammar_check
import logging
from nltk import word_tokenize, sent_tokenize, pos_tag
from nltk.corpus import wordnet as wn
from nltk import ne_chunk
from nltk.tree import Tree
import operator
import re
import service
from app import dao
from flask import request
import datetime
from utils import textUtil
from scipy.spatial import distance
from bson import ObjectId
from math import log as mathLog

tool = grammar_check.LanguageTool('en-US')

logging.basicConfig()
log = logging.getLogger(__name__)

"""
Simple string containing a dot and a whitespace
"""
DOT_PATTERN = ". "

"""
regex pattern to help remove emojis from text
"""

"""
regex pattern to remove non-alphabetic characters
"""
regex = re.compile('[^a-zA-Z]')
# regex = re.compile('[,\.!?]') #etc.

"""d
Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
"""
words = open("words-by-frequency.txt").read().split()
wordcost = dict((k, mathLog((i + 1) * mathLog(len(words)))) for i, k in enumerate(words))
maxword = max(len(x) for x in words)

"""
don't consider these errors as grammatical errors
"""
blacklistedIssues = []
blacklistedIssues.append("typographical")
blacklistedIssues.append("style")

"""
valid hashtags
"""
whitelisted_hashtags = []
whitelisted_hashtags.append("and")


def calculate_gig_keywords():
    try:
        gig = request.json
        calculateGigKeywordsAndUpdate(gig)
    except Exception as e:
        log.error(e)
        return None
    

def calculateGigKeywordsAndUpdate(gig):
    try:
        gig = extractKeywords(gig)

        """
        get latest Nlp Auth Id from the Platform Db
        """
        nlpAuthId = service.get_nlp_auth_id()
        
        service.update_gig(gig, nlpAuthId)
    except Exception as e:
        log.error(e)
        return None  


def extractKeywords(gig):
    try:
        """
        form the string from which we need to extract keywords
        """
        gigString = gig.get('gigTitle') + DOT_PATTERN + gig.get('gigDescription') + DOT_PATTERN
        
        for term in gig.get('tandc'):
            gigString = gigString + DOT_PATTERN + str(term.get('term'))
        
        """
        pre-process the string
        """
        sentences = sent_tokenize(gigString)
        sentences = [word_tokenize(sent) for sent in sentences]
        sentences = [pos_tag(sent) for sent in sentences]
    
        merged_sentences = [j for i in sentences for j in i]
        
        names = []
        orgs = set([])
     
        for tagged_sentence in sentences:
            for chunk in ne_chunk(tagged_sentence):
                if type(chunk) == Tree:
                    if chunk.label() == 'PERSON':
#                         names.add(' '.join([c[0] for c in chunk]))
                        names.extend([c[0].lower() for c in chunk if c[0].lower() not in names])
                    elif chunk.label() == 'ORGANIZATION':
                        orgs.add(' '.join([c[0].lower() for c in chunk if c[0].lower() not in names])) 

        """
        extract lower case nouns and adjectives to eliminate the extraction of similar words with different case
        For eg. Program and program
        """
        """ 
        remove names and orgs from nouns to reduce the number of extracted nouns
        """
        nouns = set([token.lower() for token, pos in merged_sentences if (pos.startswith('N') and token.lower() not in names and token.lower() not in orgs)])
        adjectives = set([token.lower() for token, pos in merged_sentences if pos.startswith('J')])
        
        """
        store the keywords inside a dict
        """
        keywords = {}
        keywords['nouns'] = list(nouns)
        keywords['adjectives'] = list(adjectives)
        keywords['names'] = list(names)
        keywords['orgs'] = list(orgs)    
                        
        print "gigString: " + gigString
        print "nouns: " + str(len(keywords['nouns'])) + " " + str(keywords['nouns']) 
        print "adjectives: " + str(len(keywords['adjectives'])) + " " + str(keywords['adjectives'])
        print "names: " + str(len(keywords['names'])) + " " + str(keywords['names'])
        print "orgs: " + str(len(keywords['orgs'])) + " " + str(keywords['orgs'])
        
        """
        store the gig keywords dict inside the gig object
        """
        gig['gigKeywords'] = keywords
        
        return gig
    
    except Exception as e:
        log.error(e)
        return None


def parse_all_profiles():
    try:
        for i in range(0, 200):
            skip_constant = 50
            skip = i * skip_constant
            profiles = dao.get_items("profile", None, skip)
            if len(profiles) == 0:
                print("done")
                return
            else:
                for profile in profiles:
                    # run only for fully completed profiles
                    if ('profileId' in profile and isinstance(profile.get('profileId'), basestring)):
                        profile['profileId'] = str(profile.get('_id'))
                        calculateProfileScoreAndUpdate(profile)
        
        """
        The following code is required while debugging
        """
#         query_params = {}
#         query_params['profileId'] = "5a0adae84cedfd0001bdc911"
#         profile = dao.get_all("profile", query_params)
#         calculateProfileScoreAndUpdate(profile[0])
        
    except Exception as e:
        log.error(e)
        return None  


def parse_one_profile():
    try:
        profile = request.json
        calculateProfileScoreAndUpdate(profile)
     
    except Exception as e:
        log.error(e)
        return None  


def calculateProfileScoreAndUpdate(profile):
    try:
        query_params = {}
        query_params['profileId'] = profile.get("profileId")
        
        gigs = dao.get_all("gig", query_params)
         
        val = profile.get("careerAspiration")
        if val == None:
            return   
        
        formattingErrors = 0
               
        grammaticalErrors = tool.check(val) 
        
        if (len(grammaticalErrors) > 0):
            for error in grammaticalErrors:
                if error.locqualityissuetype in blacklistedIssues:
                    formattingErrors += 1;
       
        words = val.split(' ')    
       
        about_me_dict = dict()
        about_me_dict["grammaticalErrors"] = len(grammaticalErrors) - formattingErrors
        about_me_dict["formattingErrors"] = formattingErrors
        about_me_dict["numOfWords"] = len(words)
        about_me_dict["profileConsistencyScore"] = getProfileConsistencyScore(profile)
        
        gig_dict = dict()
        gig_dict["gigCountScore"] = calculateGigCountScore(gigs)
        gig_dict["gigConsistencyScore"] = getMaxGigConsistencyScore(gigs, profile)
        
        profile_scores = dict()
        profile_scores["aboutMeScore"] = calculateAboutMeScore(about_me_dict.get('grammaticalErrors'), about_me_dict.get('numOfWords'))
        profile_scores["interestsScore"] = calculateInterestsScore(profile)
        profile_scores["hashtagsScore"] = calculateHashtagsScore(profile)
        profile_scores["aboutMeDict"] = about_me_dict
        profile_scores["gigDict"] = gig_dict
        profile_scores["educationScore"] = calculateEducationScore(profile)
        profile_scores["writtenCommunicationScore"] = calculateWrittenCommunicationScore(profile_scores)
        profile["profileSummary"] = generateProfileSummary(profile, gigs)
        print("profileSummary: " + profile.get("profileSummary"))
        
        profile_matrix = []
        profile_matrix.append(profile_scores.get("aboutMeScore"))
        profile_matrix.append(profile_scores.get("interestsScore"))
        profile_matrix.append(profile_scores.get("hashtagsScore"))
        profile_matrix.append(gig_dict.get("gigCountScore"))
        profile_matrix.append(profile_scores.get("educationScore"))
        profile_matrix.append(about_me_dict.get("profileConsistencyScore"))
        profile_matrix.append(gig_dict.get("gigConsistencyScore"))
        
        # ["aboutMeScore", "interestsScore", "hashtagsScore", "gigCountScore", "educationScore", "profileConsistencyScore", "gigConsistencyScore"]
        # [8,               5,                7,                6,                3,                1,                        1]
        ideal_matrix = service.get_distance_matrix()
      
        """
        _id is absent in the profile so creating one using profileId
        """
        id_str = profile.get("profileId")
        
        """
        converting _id to Object
        """
        oid = ObjectId(id_str)
        
        profile_scores['distanceMatrixScore'] = calculateDistanceMatrixScore(profile_matrix, ideal_matrix)
        profile_scores["calculatedDateTime"] = datetime.datetime.now()
        
#         print "name: " + str(profile.get('fullName'))
#         print "profile matrix: " + str(profile_matrix)
#         print "ideal matrix: " + str(ideal_matrix)
#         print "distance matrix: " + str(profile_scores['distanceMatrixScore'])
#          
#         return
        
#         print "profileConsistency: " + str(about_me_dict["profileConsistencyScore"])
#         print "aboutMeScore: " + str(profile_scores["aboutMeScore"])
#         print "distanceMatrixScore: " + str(profile_scores['distanceMatrixScore'])
#         
#         return
        
        profile["profileScores"] = profile_scores
        
        dao.update("profile", profile, oid)  
    except Exception as e:
        print(e)

    
def calculateDistanceMatrixScore(profile_matrix, ideal_matrix):
    return distance.euclidean(profile_matrix, ideal_matrix)


def getMaxGigConsistencyScore(gigs, profile):
    try: 
        gigScores = []
        if len(gigs) == 0:
            return 0
        else:
            for gig in gigs:
                gigScores.append(getGigConsistencyScore(gig, profile))
     
        index, value = max(enumerate(gigScores), key=operator.itemgetter(1))
        return value
    except Exception as e:
        print(e)

        
def calculateGigCountScore(gigs):
    number_of_gigs = len(gigs)
    if number_of_gigs >= 5:
        return 6
    elif number_of_gigs >= 2 and number_of_gigs <= 4:
        return 4
    elif number_of_gigs == 1:
        return 2
    else:
        return 0

'''
     method which returns the written communication score for the profile
'''


def calculateWrittenCommunicationScore(profile_scores):
    return (profile_scores["aboutMeScore"] * 5) + (profile_scores["hashtagsScore"] * 2) 
    
'''
     method which returns the consistency score for the profile
'''


def getGigConsistencyScore(item, profile):
    focus_sentence = item.get("gigSummary")
    if not focus_sentence:
        return 0.0
    focus_sentence = remove_non_ascii(focus_sentence)
    sent = []
    if 'gigHashtags'in item.keys():
        sentences = item.get("gigHashtags")
        for items in sentences:
            sent.append(items.replace("#", ""))
             
    """ separate out hashtags into individual and separate words
    to help check for consistency
    """
     
    if 'userHashtags'in profile.keys():
        sentences = profile.get("userHashtags")
        for items in sentences:
            items = items.replace("#", "")
            tokenized_item = word_tokenize(items)
          
            try:
                for token in tokenized_item:
                    token_with_space = infer_spaces(regex.sub('', token.lower()))
                    if (len(token_with_space) > 1):
                        sent.extend(token_with_space)
                    else:
                        sent.append(token_with_space[0])
            except Exception as e:
                print e
    
    if 'interests'in profile.keys():       
        sent.extend(profile.get("interests"))
        
    sent.extend(word_tokenize(profile.get('careerAspiration')))
         
    scores = []
    if len(sent) == 0:
        return 0.0
    else:
        for sentence in sent:
            scores.append(textUtil.symmetric_sentence_similarity(focus_sentence, sentence))
    index, value = max(enumerate(scores), key=operator.itemgetter(1))
    return value

'''
    method which returns the consistency score for the profile
'''


def getProfileConsistencyScore(item):
    focus_sentence = item.get("careerAspiration")
    if not focus_sentence:
        return 0.0
    focus_sentence = remove_non_ascii(focus_sentence)
    sent = []
    
    """ separate out hashtags into individual and separate words
    to help check for consistency
    """
    
    if 'userHashtags'in item.keys():
        sentences = item.get("userHashtags")
        for items in sentences:
            items = items.replace("#", "")
            tokenized_item = word_tokenize(items)
         
            try:
                for token in tokenized_item:
                    token_with_space = infer_spaces(regex.sub('', token.lower()))
                    if (len(token_with_space) > 1):
                        sent.extend(token_with_space)
                    else:
                        sent.append(token_with_space[0])
            except Exception as e:
                print e

    if 'interests'in item.keys():       
        sent.extend(item.get("interests"))
         
    scores = []
    if len(sent) == 0:
        return 0.0
    else:
        for sentence in sent:
            scores.append(textUtil.symmetric_sentence_similarity(focus_sentence, sentence))
    index, value = max(enumerate(scores), key=operator.itemgetter(1))
    return value

    
def calculateEducationScore(profile):
    try:
        educationDict = profile.get('education')
        graduationYear = int(educationDict.get("graduationYear"))
        currentDateTime = datetime.datetime.now()
        currentYear = int(currentDateTime.year)
        
        # Final year student
        if (graduationYear - currentYear) == 0:
            return 2
        # Non - final year student
        elif (graduationYear - currentYear) > 0:
            return 3
        # Not a student
        else:
            return 1
    except Exception:
        return 0


def generateProfileSummary(profile, gigs):
    result = ""
    try:
        
        # Get the profile's updated date
        updatedDate = profile.get("updatedDate").date()
        # Create a new reference date which tells the program that any user who has
        # updted his profile after this date, should be treated as a NEW user
        referenceDate = datetime.date(2019, 12, 25)
        
        # New User
        if (updatedDate > referenceDate):
                # add skills
            if 'userHashtags' in profile.keys() and len(profile.get("userHashtags")) > 0:
                userHashtags = profile.get("userHashtags")
                result += "A "
                if (len(userHashtags) > 1):
                    for userHashtag in userHashtags[:-1]:
                        result += str(userHashtag) + ", "  
                    result += "and " + str(userHashtags[len(userHashtags) - 1]) + ". "
                else:
                    result += str(userHashtags[len(userHashtags) - 1]) + ". "  
        
        # add address        
        result += "Residing in " + profile.get("location").get("name") + ". "
        
        # add interests    
        if 'interests' in profile.keys() and len(profile.get("interests")) > 0:
            interests = profile.get("interests")
            result += "Interested in "
            if (len(interests) > 1):
                for interest in interests[:-1]:
                    result += str(interest) + ", "  
                result += "and " + str(interests[len(interests) - 1]) + ". "
            else:
                result += str(interests[len(interests) - 1]) + ". "    
        
        # add gigs    
        if len(gigs) > 0:
            result += "Previous gigs include "
            if (len(gigs) > 1):
                for gig in gigs[:-1]:
                    result += str(gig.get("gigTitle")) + ", "
                result += "and " + str(gigs[len(gigs) - 1].get("gigTitle")) + ". "
            else:
                result += str(gigs[len(gigs) - 1].get("gigTitle")) + ". "
            
        # add education       
        educationDict = profile.get('education')
        graduationYear = int(educationDict.get("graduationYear"))
        currentYear = int(datetime.datetime.now().year)
        
        # student
        if (graduationYear - currentYear) >= 0:
            result += "Studying " + str(educationDict.get("course")) + " from " + str(educationDict.get("college")) + " and graduating in " + str(graduationYear) + "."
        # Not a student
        else:
            result += "Studied " + str(educationDict.get("course")) + " from " + str(educationDict.get("college")) + " and graduated in " + str(graduationYear) + "."
    except Exception as e:
        log.error(e)
    return result
    
    
def calculateInterestsScore(profile):
    if 'interests' in profile.keys(): 
        if len(profile.get("interests")) > 3:
            return 5
        elif len(profile.get("interests")) == 2 or len(profile.get("interests")) == 3:
            return 3
        elif len(profile.get("interests")) == 1:
            return 1
        return 0 
    else: 
        return 0
    

def calculateHashtagsScore(profile):
    if 'userHashtags' in profile.keys():
        validHashtags = getValidHashtagsCount(profile.get("userHashtags"))
        if validHashtags >= 7:
            return 7 
        else:
            return  validHashtags
    else: 
        return 0


def getValidHashtagsCount(userHashtags):
    result = 0
    for item in userHashtags:
        tokenized_item = word_tokenize(item)
        
        if (len(tokenized_item) == 1):
            token_with_space = infer_spaces(regex.sub('', tokenized_item[0].lower()))
            if (len(token_with_space) > 1):
                tokenized_item = token_with_space
        
        if len(tokenized_item) > 1:
            """
            flag to check whether all the words in hashtag are valid
            if all words are valid, increase valid count by one
            otherwise break and check the next hashtag 
            """
            
            allTokensValid = True
            for token in tokenized_item:
                if not wn.synsets(token) and token.lower() not in whitelisted_hashtags:
                    """
                    Not an English Word
                    """
                    allTokensValid = False
                    break
            if (allTokensValid):
                result += 1
        else:
            if not wn.synsets(item):
                """
                Not an English Word
                """
                continue
            else:
                """
                English Word
                """
                result += 1
    
    return result


def calculateAboutMeScore(errors, numOfWords):
    try:
        conversionFactor = 1
        if numOfWords <= 5:
            conversionFactor = 0.5
            if errors >= 5 :
                return 0
            else:
                sc = float(errors * 0.2)
                return float((1 - sc) * conversionFactor * 8)
        elif numOfWords > 5 and numOfWords < 20:
            conversionFactor = 0.75
            if errors >= 10 :
                return 0
            else:
                sc = float(errors * 0.1)
                return float((1 - sc) * conversionFactor * 8)
        elif numOfWords >= 20:
            if errors >= 20 :
                return 0
            else:
                sc = float(errors * 0.05)
                return float((1 - sc) * 8)
                
    except Exception as e:
        log.error(e)
        return None

'''
     method to remove non ascii characters from text   
'''


def remove_non_ascii(text):
    return re.sub(r'[^\x00-\x7F]', ' ', text)

'''
    method which separates out the hashtags which don't contain any space
    and returns individual words in an array
'''


def infer_spaces(s):
    """Uses dynamic programming to infer the location of spaces in a string
    without spaces."""

    """
    Find the best match for the i first characters, assuming cost has
    been built for the i-1 first characters.
    Returns a pair (match_cost, match_length).
    """

    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i - maxword):i]))
        return min((c + wordcost.get(s[i - k - 1:i], 9e999), k + 1) for k, c in candidates)

    """
    Build the cost array.
    """
    cost = [0]
    for i in range(1, len(s) + 1):
        c, k = best_match(i)
        cost.append(c)

    """
    Backtrack to recover the minimal-cost string.
    """
    out = []
    i = len(s)
    while i > 0:
        c, k = best_match(i)
        assert c == cost[i]
        out.append(s[i - k:i])
        i -= k

    return out
 
